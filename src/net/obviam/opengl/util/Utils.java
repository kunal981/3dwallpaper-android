package net.obviam.opengl.util;

import java.util.Random;

import net.obviam.opengl.R;

public class Utils {

	private static int[] imageDalekId = { // Image file IDs
	R.drawable.s_dalek, R.drawable.a_dalek, R.drawable.b_dalek,
			R.drawable.c_dalek, R.drawable.e_dalek, R.drawable.d_dalek,
			R.drawable.f_dalek

	};

	public static int getDalekSId() {

		Random random = new Random();
		int position = random.nextInt(imageDalekId.length);
		return imageDalekId[position];

	}

}
