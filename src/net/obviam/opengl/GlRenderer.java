/**
 * 
 */
package net.obviam.opengl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

/**
 * @author impaler
 * 
 */
public class GlRenderer implements Renderer {

	private float yrot; // Y Rotation ( NEW )
	private float xtrans;
	private float ytrans;

	public static final int R_UP = 1;
	public static final int R_DOWN = 2;

	private Square square, square2, square3, square4;// the square
	private Square[] squares;// the square

	private Background background;
	private Context context;

	/** Is twinkle enabled? */
	private boolean twinkle = false;

	private final int num = 10;
	private Star star;
	private Star2 star2;
	private StarThree star3;
	private Star4 star4;
	private Star5 star5;
	private Star[] stars;
	private Cube cube;
	private PhotoCube photocube;
	private DalekGaurd dGaurd;

	private static float anglePyramid = 0; // Rotational angle in degree for
											// pyramid (NEW)
	private static float angleCube = 0; // Rotational angle in degree for cube
										// (NEW)
	private static float speedPyramid = 2.0f; // Rotational speed for pyramid
												// (NEW)
	private static float speedCube = -1.5f; // Rotational speed for cube (NEW)
	public static boolean f_r_1_status = true;
	public static boolean f_r_2_status = true;
	public static boolean f_r_3_status = false;
	public static boolean f_r_4_status = false;
	public static boolean f_r_5_status = false;

	/** Constructor to set the handed over context */
	public GlRenderer(Context context) {
		this.context = context;

		background = new Background();
		photocube = new PhotoCube(this.context);

		cube = new Cube();
		// dGaurd = new DalekGaurd();
		star = new Star();
		star2 = new Star2();
		star3 = new StarThree();
		star4 = new Star4();
		star5 = new Star5();

	}

	@Override
	public void onDrawFrame(GL10 gl) {
		// clear Screen and Depth Buffer
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

		background.draw(gl);
		photocube.draw(gl);
		// dGaurd.draw(gl);

		// Draw the cube (NEW)
		cube.draw(gl);
		star3.draw(gl, f_r_3_status);
		star4.draw(gl, f_r_4_status);
		star5.draw(gl, f_r_5_status);
		// square.draw(gl);
		star.draw(gl, f_r_1_status);
		star2.draw(gl, f_r_2_status);

		// Update the rotational angle after each refresh (NEW)

	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		if (height == 0) { // Prevent A Divide By Zero By
			height = 1; // Making Height Equal One
		}

		gl.glViewport(0, 0, width, height); // Reset The Current Viewport
		gl.glMatrixMode(GL10.GL_PROJECTION); // Select The Projection Matrix
		gl.glLoadIdentity(); // Reset The Projection Matrix

		// Calculate The Aspect Ratio Of The Window
		GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f,
				100.0f);

		gl.glMatrixMode(GL10.GL_MODELVIEW); // Select The Modelview Matrix
		gl.glLoadIdentity(); // Reset The Modelview Matrix
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {

		background.loadGLTexture(gl, this.context);
		photocube.loadTexture(gl);
		// dGaurd.loadGLTexture(gl, context);
		cube.loadGLTexture(gl, this.context);
		star.loadGLTexture(gl, this.context);
		star2.loadGLTexture(gl, this.context);
		star3.loadGLTexture(gl, this.context);
		star4.loadGLTexture(gl, this.context);
		star5.loadGLTexture(gl, this.context);
		gl.glEnable(GL10.GL_TEXTURE_2D); // Enable Texture Mapping ( NEW )
		gl.glShadeModel(GL10.GL_SMOOTH); // Enable Smooth Shading
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f); // Black Background
		gl.glClearDepthf(1.0f); // Depth Buffer Setup
		gl.glEnable(GL10.GL_DEPTH_TEST); // Enables Depth Testing
		gl.glDepthFunc(GL10.GL_LEQUAL); // The Type Of Depth Testing To Do

		// Really Nice Perspective Calculations
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);

	}

	public void handleRotation(int ROTATION_CODE) {

		if (ROTATION_CODE == R_UP) {
			float rTempSpin = square.getrSpin();
			if (rTempSpin <= 15.0f) {
				square.setrSpin(square.getrSpin() + 2.0f);
			}
		} else if (ROTATION_CODE == R_DOWN) {
			float rTempSpin = square.getrSpin();
			if (rTempSpin > 2.0f) {
				square.setrSpin(square.getrSpin() - 2.0f);
			}
		}
	}
}
