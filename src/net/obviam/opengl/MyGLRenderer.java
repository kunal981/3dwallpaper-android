package net.obviam.opengl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLU;

public class MyGLRenderer implements MyGLSurfaceView.Renderer {

	// private Pyramid pyramid; // (NEW)
	private Cube cube; // (NEW)

	/** Angle For The Pyramid */
	private float rtri;
	/** Angle For The Cube */
	private float rquad;

	/** The Activity Context ( NEW ) */
	private Context context;

	// Constructor
	public MyGLRenderer(Context context) {
		// Set up the buffers for these shapes
		// pyramid = new Pyramid(); // (NEW)
		this.context = context;
		cube = new Cube(); // (NEW)
	}

	/**
	 * The Surface is created/init()
	 */
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {

		// Load the texture for the cube once during Surface creation
		cube.loadGLTexture(gl, this.context);

		gl.glEnable(GL10.GL_TEXTURE_2D); // Enable Texture Mapping ( NEW )
		gl.glShadeModel(GL10.GL_SMOOTH); // Enable Smooth Shading
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f); // Black Background
		gl.glClearDepthf(1.0f); // Depth Buffer Setup
		gl.glEnable(GL10.GL_DEPTH_TEST); // Enables Depth Testing
		gl.glDepthFunc(GL10.GL_LEQUAL); // The Type Of Depth Testing To Do

		// Really Nice Perspective Calculations
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);

		// this is when we use colour as a background
		// gl.glShadeModel(GL10.GL_SMOOTH); // Enable Smooth Shading
		// gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f); // Black Background
		// gl.glClearDepthf(1.0f); // Depth Buffer Setup
		// gl.glEnable(GL10.GL_DEPTH_TEST); // Enables Depth Testing
		// gl.glDepthFunc(GL10.GL_LEQUAL); // The Type Of Depth Testing To Do
		//
		// // Really Nice Perspective Calculations
		// gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
	}

	/**
	 * Here we do our drawing
	 */
	public void onDrawFrame(GL10 gl) {
		
		cube.draw(gl); // Draw the Cube

		// // Reset The Current Modelview Matrix
		// gl.glLoadIdentity();
		//
		// gl.glTranslatef(0.0f, 1.3f, -6.0f); // Move up 1.3 Units and -6.0 as
		// the
		// // origin matrix is loaded before
		// gl.glRotatef(rtri, 0.0f, 1.0f, 0.0f); // Rotate The Triangle On The Y
		// // axis
		// pyramid.draw(gl); // Draw the Pyramid

		// Rotation
		// rtri += 0.2f; // Increase The Rotation Variable For The Pyramid
		
	}

	/**
	 * If the surface changes, reset the view
	 */
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		if (height == 0) { // Prevent A Divide By Zero By
			height = 1; // Making Height Equal One
		}

		gl.glViewport(0, 0, width, height); // Reset The Current Viewport
		gl.glMatrixMode(GL10.GL_PROJECTION); // Select The Projection Matrix
		gl.glLoadIdentity(); // Reset The Projection Matrix

		// Calculate The Aspect Ratio Of The Window
		GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f,
				100.0f);

		gl.glMatrixMode(GL10.GL_MODELVIEW); // Select The Modelview Matrix
		gl.glLoadIdentity(); // Reset The Modelview Matrix
	}

}
