package net.obviam.opengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class MyGLSurfaceView extends GLSurfaceView {
	Bitmap bitmap;

	// public MyGLSurfaceView(Context context) {
	// super(context);
	//
	// }

	public MyGLSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	// @Override
	// protected void onDraw(Canvas canvas) {
	//
	// canvas.drawBitmap(bitmap, 0, 0, null);
	// }

	// @Override
	// public void surfaceCreated(SurfaceHolder holder) {
	// Bitmap background = BitmapFactory.decodeResource(getResources(),
	// R.drawable.image_background);
	// float scale = (float) background.getHeight() / (float) getHeight();
	// int newWidth = Math.round(background.getWidth() / scale);
	// int newHeight = Math.round(background.getHeight() / scale);
	// bitmap = Bitmap.createScaledBitmap(background, newWidth, newHeight,
	// true);
	// }
}
