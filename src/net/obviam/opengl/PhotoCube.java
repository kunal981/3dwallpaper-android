package net.obviam.opengl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

public class PhotoCube {

	StarThree s3;

	private FloatBuffer vertexBuffer; // Vertex Buffer
	private FloatBuffer textureBuffer; // Texture Coords Buffer
	private ByteBuffer indexBuffer;

	private int numFaces = 6;
	private int[] imageFileIDs = { // Image file IDs
	R.drawable.dalek_b, R.drawable.dalek, R.drawable.dalek, R.drawable.dalek,
			R.drawable.dalek, R.drawable.dalek };

	private int[] textureIDs = new int[numFaces];
	private Bitmap[] bitmap = new Bitmap[numFaces];
	private float cubeHalfSize = 1.2f;

	private static float angleCube; // rotational angle in degree for cube
	private static float speedCube = -1.5f;

	private float[] vertices = { // Vertices of the 6 faces
	// FRONT
			-1.0f, -1.0f, 1.0f, // 0. left-bottom-front
			1.0f, -1.0f, 1.0f, // 1. right-bottom-front
			-.50f, 2.0f, .50f, // 2. left-top-front
			.50f, 2.0f, .50f, // 3. right-top-front
			// BACK
			1.0f, -1.0f, -1.0f, // 6. right-bottom-back
			-1.0f, -1.0f, -1.0f, // 4. left-bottom-back
			0.50f, 2.0f, -.50f, // 7. right-top-back
			-0.50f, 2.0f, -.50f, // 5. left-top-back
			// LEFT
			-1.0f, -1.0f, -1.0f, // 4. left-bottom-back
			-1.0f, -1.0f, 1.0f, // 0. left-bottom-front
			-.50f, 2.0f, -0.50f, // 5. left-top-back
			-.50f, 2.0f, .50f, // 2. left-top-front
			// RIGHT
			1.0f, -1.0f, 1.0f, // 1. right-bottom-front
			1.0f, -1.0f, -1.0f, // 6. right-bottom-back
			.50f, 2.0f, 0.50f, // 3. right-top-front
			.50f, 2.0f, -0.50f, // 7. right-top-back
			// TOP
			-0.50f, 0.50f, 0.50f, // 2. left-top-front
			.50f, 0.50f, 0.50f, // 3. right-top-front
			-0.50f, 0.50f, -0.50f, // 5. left-top-back
			0.50f, 0.50f, -0.50f, // 7. right-top-back
			// BOTTOM
			-1.0f, -1.0f, -1.0f, // 4. left-bottom-back
			1.0f, -1.0f, -1.0f, // 6. right-bottom-back
			-1.0f, -1.0f, 1.0f, // 0. left-bottom-front
			1.0f, -1.0f, 1.0f // 1. right-bottom-front
	};

	/** The initial texture coordinates (u, v) */
	private float texture[] = {
			//

			0.22f, 1.0f, .75f, 1.0f, 0.22f, 0.0f, .75f, 0.0f,

			0.22f, 1.0f, .75f, 1.0f, 0.22f, 0.0f, .75f, 0.0f,

			0.22f, 1.0f, .75f, 1.0f, 0.22f, 0.0f, .75f, 0.0f,

			0.22f, 1.0f, .75f, 1.0f, 0.22f, 0.0f, .75f, 0.0f,
			//
			0.22f, 1.0f, .75f, 1.0f, 0.22f, 0.0f, .75f, 0.0f,

			0.22f, 1.0f, .75f, 1.0f, 0.22f, 0.0f, .75f, 0.0f,

	};

	/** The initial indices definition */
	private byte indices[] = {
			// Faces definition
			0, 1, 3, 0, 3,
			2, // Face front
			4, 5, 7, 4, 7,
			6, // Face right
			8, 9, 11, 8, 11,
			10, // ...
			12, 13, 15, 12, 15, 14, 16, 17, 19, 16, 19, 18, 20, 21, 23, 20, 23,
			22, };

	public PhotoCube(Context context) {
		s3 = new StarThree();
		ByteBuffer byteBuf = ByteBuffer.allocateDirect(12 * 4 * numFaces);
		byteBuf.order(ByteOrder.nativeOrder());
		vertexBuffer = byteBuf.asFloatBuffer();
		vertexBuffer.put(vertices); // Copy data into buffer
		vertexBuffer.position(0); // Rewind

		//
		byteBuf = ByteBuffer.allocateDirect(texture.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		textureBuffer = byteBuf.asFloatBuffer();
		textureBuffer.put(texture);
		textureBuffer.position(0);

		//
		indexBuffer = ByteBuffer.allocateDirect(indices.length);
		indexBuffer.put(indices);
		indexBuffer.position(0);

		// Read images. Find the aspect ratio and adjust the vertices
		// accordingly.
		for (int face = 0; face < numFaces; face++) {
			bitmap[face] = BitmapFactory.decodeStream(context.getResources()
					.openRawResource(imageFileIDs[face]));
		}

	}

	// Render the shape
	public void draw(GL10 gl) {

		gl.glLoadIdentity(); // Reset the model-view matrix
		gl.glTranslatef(0.0f, 0.1f, -7.0f); // Move down 1.0 Unit And Into The
		// Screen 7.0
		// Minor change: Scale the Cube to 80 percent, otherwise it would be too
		// large for the Emulator screen
		gl.glScalef(0.5f, 0.5f, 0.5f);
		gl.glRotatef(angleCube, 0.0f, 1.0f, 0.0f);

		// Update the rotational angle after each refresh.
		angleCube += 1.95f;
		if (angleCube > 180.0f && angleCube < 182.0) {
			GlRenderer.f_r_3_status = true;
			// GlRenderer.f_r_4_status = true;
			// GlRenderer.f_r_5_status = true;
		}
		if (angleCube > 190.0f && angleCube < 195.0) {
			GlRenderer.f_r_5_status = true;
		}
		if (angleCube > 200.0f && angleCube < 205.0) {
			GlRenderer.f_r_4_status = true;
		}

		if (angleCube > 300.0f) {

			GlRenderer.f_r_1_status = true;
			GlRenderer.f_r_2_status = true;
		}
		if (angleCube > 360.0f) {
			angleCube = 0.0f;
		}

		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glFrontFace(GL10.GL_CCW);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);

		// Draw the vertices as triangles, based on the Index Buffer information
		for (int face = 0; face < numFaces; face++) {
			gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[face]);
			indexBuffer.position(6 * face);
			// gl.glDrawElements(GL10.GL_TRIANGLES, indices.length,
			// GL10.GL_UNSIGNED_BYTE, indexBuffer);
			gl.glDrawElements(GL10.GL_TRIANGLES, 6, GL10.GL_UNSIGNED_BYTE,
					indexBuffer);
		}

		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

	}

	// Load images into 6 GL textures
	public void loadTexture(GL10 gl) {
		gl.glGenTextures(6, textureIDs, 0); // Generate texture-ID array for 6
											// IDs

		// Generate OpenGL texture images
		for (int face = 0; face < numFaces; face++) {
			gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[face]);
			// Build Texture from loaded bitmap for the currently-bind texture
			// ID
			// Create Nearest Filtered Texture
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
					GL10.GL_NEAREST);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
					GL10.GL_LINEAR);

			// Different possible texture parameters, e.g. GL10.GL_CLAMP_TO_EDGE
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
					GL10.GL_REPEAT);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
					GL10.GL_REPEAT);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap[face], 0);
			bitmap[face].recycle();
		}
	}

}
