package net.obviam.opengl;

import android.app.Activity;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Run extends Activity {

	/** The OpenGL view */
	private GLSurfaceView glSurfaceView;
	private Button bUp, bDown, bNext;
	GlRenderer renderer;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// requesting to turn the title OFF
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// making it full screen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.main);

		renderer = new GlRenderer(this);
		// Initiate the Open GL view and
		// create an instance with this activity
		// glSurfaceView = new MyGLSurfaceView(this);
		glSurfaceView = (MyGLSurfaceView) findViewById(R.id.surfaceviewclass);
		// glSurfaceView = (MyGLSurfaceView)
		// findViewById(R.id.surfaceviewclass);
//		glSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
//		glSurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
		glSurfaceView.setZOrderOnTop(true);
		// glSurfaceView.setBackgroundResource(R.drawable.background);
		// set our renderer to be the main renderer with
		// the current activity context
		glSurfaceView.setRenderer(renderer);
		glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

		// setContentView(glSurfaceView);

		// bUp = (Button) findViewById(R.id._rUp);
		// bUp.setOnClickListener(this);
		// bDown = (Button) findViewById(R.id._rDp);
		// bDown.setOnClickListener(this);
		// bNext = (Button) findViewById(R.id._cBackGround);
		// bNext.setOnClickListener(this);

		// glSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
		// glSurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
	}

	/**
	 * Remember to resume the glSurface
	 */
	@Override
	protected void onResume() {
		super.onResume();
		glSurfaceView.onResume();
	}

	/**
	 * Also pause the glSurface
	 */
	@Override
	protected void onPause() {
		super.onPause();
		glSurfaceView.onPause();
	}

	// @Override
	// public void onClick(View v) {
	//
	// int id = v.getId();
	// switch (id) {
	// case R.id._rUp:
	// renderer.handleRotation(1);
	// break;
	// case R.id._rDp:
	// renderer.handleRotation(2);
	//
	// break;
	// case R.id._rControl:
	//
	// break;
	//
	// default:
	// break;
	// }
	//
	// }

}